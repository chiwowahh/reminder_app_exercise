class ReminderMailer < ApplicationMailer
  default from: 'reminder@example.com'

  def reminder_email(reminder, user)
    @reminder = reminder
    mail(to: user.email, subject: 'Reminder')
  end
end
