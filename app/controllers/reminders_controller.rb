class RemindersController < ApplicationController
  before_action :authenticate_user!, :reminders_facade
  delegate :index, to: :reminders_facade

  def reminders_facade
    @facade = RemindersFacade.new(params,current_user)
  end

  def create
    if reminders_facade.create
      redirect_to reminders_facade.reminder, notice: 'Reminder created'
    else
      render :new
    end
  end

  def destroy
    if reminders_facade.destroy
      redirect_to reminders_url, notice: 'Reminder was successfully destroyed.'
    else
      render :destroy
    end
  end

  def update
    if reminders_facade.update
      redirect_to reminders_facade.reminder, notice: 'Reminder was successfully updated.'
    else
      render :edit
    end
  end
end
