class Reminder < ApplicationRecord
  attr_accessor :day_rule, :month, :year, :hour, :minute
  belongs_to :user
  validate :input_day, :in_future, if: :input_date
  validates :title, :text, :month, :year, :hour, :minute, presence: true

  VALID_DATE_RULES = [
      'of month',
      'last of month'
  ].freeze


  def parse_date
    self.remind_on = DateTime.new(year.to_i,
                                  month.to_i,
                                  parse_day,
                                  hour.to_i,
                                  minute.to_i,
                                  0,
                                  Time.now.zone)
  end

  private

  def input_day
    valid_input = VALID_DATE_RULES.map do |rule|
      day_rule.include?(rule)
    end
    errors.add :day_rule, 'is invalid' unless valid_input.include?(true)
  end

  def in_future
    errors.add :day_rule, 'is in the past' if parse_date.to_time <= Time.now
  end

  def input_date
    begin
      Date.new(year.to_i, month.to_i, parse_day)
    rescue StandardError
      errors.add :day_rule, 'date invalid'
      return false
    end
  end

  def parse_day
    day = day_rule.split(/\W+/)[0].to_i
    return calculate_day(day) if day_rule.include?('last')

    day
  end

  def calculate_day(day)
    Time.days_in_month(month.to_i, year.to_i) - subtract_day(day)
  end

  def subtract_day(day)
    day.zero? ? day : day - 1
  end
end