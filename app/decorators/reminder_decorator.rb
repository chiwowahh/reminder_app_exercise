class ReminderDecorator
  def day_rule
    days = []
    days << "last of month"

    5.times do |day|
      day += 1
      days << day.ordinalize + " last of month" if day > 1
    end

    days_in_month = Time.days_in_month(12,2018) - 1
    days_in_month.times do |day|
      day += 1
      days << day.ordinalize + " of month"
    end

    days
  end
end