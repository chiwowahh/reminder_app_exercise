class RemindersFacade
  attr_reader :params, :current_user, :reminder

  def initialize(params, current_user)
    @params = params
    @current_user = current_user
    reminder_decorator
  end

  def index
    reminders
  end

  def create
    new_reminder = Reminder.new(safe_params.merge(user_id: current_user.id))
    if new_reminder.save
      send_mail(new_reminder)
      true
    else
      reminder.errors.add :whoops, 'something went wrong'
      false
    end
  end

  def update
    reminder.update(safe_params)
  end

  def destroy
    reminder.destroy
  end

  def reminders
    @reminders ||= Reminder.where(user_id: current_user.id)
  end

  def reminder
    return @reminder ||= find_reminder if reminder_id

    @reminder ||= Reminder.new
  end

  def reminder_decorator
    @reminder_decorator ||= ReminderDecorator.new
  end

  private

  def send_mail(reminder)
    ReminderMailer.reminder_email(reminder, current_user)
                  .deliver_later(wait_until: reminder.remind_on.to_time.to_f)
  end

  def safe_params
    params.require(:reminder).permit(:title, :text, :day_rule, :month, :year, :minute, :hour).to_h
  end

  def reminder_param
    params[:reminder]
  end

  def reminder_id
    return reminder_param[:id] if reminder_param.present? &&
                                  reminder_param[:id].present?

    params[:id]
  end

  def find_reminder
    Reminder.find_by(id: reminder_id, user_id: current_user.id)
  end

end