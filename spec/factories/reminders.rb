FactoryBot.define do
  factory :reminder do
    association :user
    title "title"
    text "text"
    year 2018
    month 12
    hour 12
    minute 15
    day_rule 'last of month'
  end
end