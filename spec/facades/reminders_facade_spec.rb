require 'rails_helper'
require 'spec_helper'

describe RemindersFacade do
  include ActiveJob::TestHelper
  let!(:user) { create(:user) }
  let!(:reminder) { create(:reminder, user: user) }

  before do
    now = Time.parse('2018-12-23 12:00:00')
    allow(Time).to receive(:now) { now }
  end

  describe '#index' do
    it 'retrieves all reminders for current user' do
      expect(facade({}).index.size).to eq 1
    end
  end

  describe '#create' do
    it 'creates a new Reminder and send email' do
      count = Reminder.count

      message_delivery = instance_double(ActionMailer::MessageDelivery)
      expect(ReminderMailer).to receive(:reminder_email)
        .and_return(message_delivery)
      allow(message_delivery).to receive(:deliver_later)

      expect(facade(xmas).create).to be_truthy
      expect(Reminder.count).to eq count + 1
    end

    it 'fails if Reminder date is in the past' do
      count = Reminder.count
      expect(facade(too_late).create).to be_falsey
      expect(Reminder.count).to eq count
    end
  end

  describe '#update' do
    it 'should change a Reminder' do
      reminder = Reminder.first
      params = xmas
      params[:reminder][:title] = 'Christmas'
      params[:reminder][:id] = reminder.id

      expect(reminder.title).to_not eq 'Christmas'
      expect(facade(params).update).to be_truthy
      expect(Reminder.find(reminder.id).title).to eq 'Christmas'
    end

    it 'should not change a Reminder if date is in the past' do
      reminder = Reminder.first
      params = too_late
      params[:reminder][:title] = 'Xmas'
      params[:reminder][:id] = reminder.id

      expect(facade(params).update).to be_falsey
      expect(Reminder.find(reminder.id).title).to eq reminder.title
    end
  end

  describe '#destroy' do
    it 'should destroy a reminder' do
      reminder = Reminder.first
      params = xmas
      params[:reminder][:id] = reminder.id
      count = Reminder.count - 1

      expect(facade(params).destroy).to be_truthy
      expect(count).to eq Reminder.count
    end
  end

  private

  def new_params(params)
    ActionController::Parameters.new(params)
  end

  def facade(params)
    RemindersFacade.new(params, user)
  end

  def xmas
    new_params(reminder: { title: 'Xmas',
                           text: 'meet and greet with Santa',
                           year: 2018,
                           month: 12,
                           hour: 12,
                           minute: 15,
                           day_rule: '24th of month' })
  end

  def too_late
    new_params(reminder: { title: 'Xmas',
                           text: 'meet and greet with Santa',
                           year: 2018,
                           month: 12,
                           hour: 12,
                           minute: 15,
                           day_rule: '21th of month' })
  end
end