class CreateReminders < ActiveRecord::Migration[5.0]
  def change
    create_table :reminders do |t|
      t.references :user, index: true
      t.string :title
      t.string :text
      t.datetime :remind_on
      t.timestamps
    end
  end
end
